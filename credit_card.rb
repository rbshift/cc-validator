class CreditCard
  attr_accessor :number, :type, :validity
  
  def self.card_type_matches
    # First six digits of a card number (IIN)
    # American Express: 37****
    # Discover: 65****
    # Mastercard: 50****, 51****, 52****, 53****, 54****, 55****
    # Visa: 40****, 41****, 42****, 43****, 44****, 45****, 46****, 47****, 48****, 49****
    {
      :american_express => /37\d{4}/,
      :discover => /65\d{4}/,
      :mastercard => /5[0,5]\d{4}/,
      :visa => /4[0,9]\d{4}/
    }
  end
  
  def initialize(number, type)
    @number = number
    @type = type
    @validity = false
    validation
  end

  private
    
    # Checks if the IIN (first 6 digits) matches the card provider patterns
    def iin_valid(number)
      case @type
      when :american_express
        number =~ (CreditCard.card_type_matches[:american_express])
      when :discover
        number =~ (CreditCard.card_type_matches[:discover])
      when :mastercard
        number =~ (CreditCard.card_type_matches[:mastercard])
      when :visa
        number =~ (CreditCard.card_type_matches[:visa])
      end
    end
    
    # Double every other account number (odd index positions)
    # Account number is the credit card number without the first six digits (IIN), and without the final digit (check digit).
    def luhn_doubling(number)
      number = number[6..-2]
      returned_number = []
      number.split('').each_with_index do |n, index|
        index.even? ? returned_number << n.to_i : returned_number << n.to_i * 2
      end
      returned_number
    end

    # Sum the digits of any number with more than 1 digit (e.g 10 => 1 + 0 => 1, 12 => 1 + 2 => 3, etc.)
    def luhn_number_summing
      sum = 0
      numbers_over_9 = luhn_doubling(number).map { |i| i.to_s}.select { |n| n.size > 1 }
      numbers_over_9.each do |number_string|
        sum += number_string.split('').map { |c| c.to_i }.inject(:+)
      end
      sum
    end

    # Combine the newly-created sums of the double-digit numbers with the original single-digit numbers
    def luhn_computation
      luhn_doubling(number).delete_if { |c| c > 10 }.inject(:+) + luhn_number_summing
    end
    
    # Generate the check digit by obtaining the last digit from the sum multiplied by 9
    def check_digit
      (luhn_computation * 9).to_s.split('').last
    end
    
    # Validate the generated check digit against the input card number's final digit
    def check_digit_valid(number)
      number[-1] == check_digit
    end
    
    def validation
      @validity = true if iin_valid(number) && check_digit_valid(number)
    end
end

cards_array = []
cards_hash = {}

def prompt
  puts "Input a credit card number and card provider (Visa, Mastercard, American Express, or Discover)"
  puts "Example input: 7777000077770000 Visa"
  puts "Input 'stop' or 'exit' to terminate input and validate the card numbers."
end

puts prompt

while line = gets.chomp.downcase
  break if (line == "stop") || (line == "exit")
  
  if (line =~ /\d{16,18}\s[A-Za-z]+\s?[A-Za-z]+/).nil?
    puts "Invalid input. Please enter a valid number and card provider."
    next
  end
  
  card_number = line.chomp.strip.match(/\d*/)[0]
  card_type = line.chomp.strip.match(/[A-Za-z]+\D[A-Za-z]+/)[0].gsub(' ', '_').downcase.to_sym
  
  cards_array << CreditCard.new(card_number, card_type) if CreditCard.card_type_matches.include?(card_type)
end

cards_array.each do |card| 
  cards_hash[card.number] = card.validity
end

cards_hash.each_pair do |k, v|
  v == true ? (puts "#{k} is possibly a valid credit card number") : (puts "#{k} is an invalid credit card number")
end